---
title: Struggles with Ryzen, Fedora, and a BIOS
date: 2019-07-07 18:26:34
categories:
- Experience
tags:
- BIOS
- Linux
- Fedora
- Bare metal
- Ryzen
- AMD
---

Last year, I built a new PC for my every day use. I initially went with a Ryzen-based PC build where I had a Ryzen 2700X as the core of the build. The motherboard I got was from Gigabyte, and I had a lot of stability issues. There would be moments where I would be on a call or working on something, and the computer would freeze and buzz, meaning that the last bit of sound that was playing would repeat over and over again. I would hear the HDD spinning up as a result, and then the machine would get back to operating fine.

I made some adjustments on the OS side to prevent the HDD from spinning up at random times, but then I would start getting BSODs. I was not sure what was causing it, but there were several applications that would cause the OS to crash, such as Zoom, Total War Warhammer, Chrome, Firefox, Microsoft Word, etc.

I was not sure if this was a motherboard issue or a processor issue, but I decided to scrap the Ryzen build and go for a modest Core i5 8600K build. Ever since going that route, I have not seen any instability.

However, I had a Ryzen 7 2700X just sitting around doing nothing. I started a new job in which would require me to have my own lab and mess with malicious PCAPs, so I thought it would be a great opportunity to start working with virtualization. I bought some new PC components to build around Ryzen again. This time, I went with an Asus motherboard, the TUF B450M-PLUS Gaming. It's an MATX motherboard with some nice features, and all I really needed was a place to put the processor. I was not planning on overclocking or doing any gaming on the machine. It was purely for learning and work purposes.

I put the PC together and installed Fedora Workstation 29 on there, which worked great for what I was trying to do. The problems did not occur until I updated to Fedora 30. As of last week, the machine has been down. There is something weird with the kernel that does not work well with Ryzen. I decided to go into the BIOS and update the firmware. This is where things fell apart.

I decided to do the BIOS update through the Internet, and the motherboard started updating the BIOS, and then it suddenly stopped. Aftera reboot, it would not POST. Going into the user manual, I read that in order to reinstall the BIOS, I would need to clear the CMOS. Doing what the guide said to do, I tried it multiple times with no success. No matter what I did, nothing would work.

At this point, I am ready to call this motherboard dead. I enjoyed having a Linux workstation, but now I am out of luck. My next step is to get a new motherboard for this CPU and install a different distro of Linux, preferably Pop_OS! from System 76. I do like Fedora a lot, and I wish I could continue using it, but after this experience, I need something a bit safer and stable for Ryzen. Pop_OS! has done well for me in the past, so I will continue using it.

If you have any questions or comments, please let me know. I would love to save this motherboard instead of buying a new one, but I don't know what else to do at this point. Reach out to me on Twitter at [@WrittenByAPanda](https://twitter.com/writtenbyapanda).
