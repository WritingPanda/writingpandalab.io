---
title: How to add SSH keys automatically
date: 2019-07-07 13:54:30
categories:
- Tutorial
tags: 
- ssh
- tutorial
- PKI
- Linux
- Ubuntu
- WSL
---

## Problem

When you start a terminal session in Linux, your SSH keys are not add to the agent, so when you want to push code to your repository of choice, you get refused because the private key is not currently in use by anything. 

## How I learned about a solution

This is something I come across every time I try a new distro or boot up another version of WSL. Right now, I am using Windows 10 Pro Insiders Preview, and I have access to [WSL 2](https://devblogs.microsoft.com/commandline/wsl-2-is-now-available-in-windows-insiders/). I have a project I started with my laptop running [Pop_OS!](https://system76.com/pop), which is a distro built and maintained by System 76 and based on Ubuntu. In order to work on the same project on two different machines, I needed to access my repository, located [here](https://gitlab.com/WritingPanda/writingpanda.gitlab.io). Knowing that I am using WSL 2, I needed to add SSH keys to GitLab before proceeding:

    ssh-keygen -o -t rsa -b 4096 -C "email@example.com"

I specified where I wanted the SSH keys to be stored, and then I needed to add the public key to GitLab, which was an easy copy and paste. From there, I had to make sure I tested that the authentication was working, and here is where I hit my initial snag:

    ssh -T git@gitlab.com

After adding the ECDSA key fingerprint, my connection was refused because the auth could not be completed. In order to make sure my SSH key would be used whenever I make a call to GitLab (or Github), I had to add the key to the `ssh-agent`. 

    eval $(which ssh-agent -s)
    ssh-add ~/.ssh/private_key_file

Afterwards, I could run the `ssh -T git@gitlab.com` and get the response I was looking for:

    Welcome to GitLab, @WritingPanda!

However, when I restarted my terminal or began a new session, I would no longer be able to connect to GitLab. Therefore, I needed a way to permanently add my SSH key to the agent every time I started a session.

I looked up some solutions and found two interesting ones that could work for you. 

## Solution 1

This is my preferred solution and the one recommended by GitLab. Using a config file in the `.ssh` folder in my home directory, I could make it so that the `ssh-agent` will use the key every time I use an ssh command. I created a config file in the `.ssh` folder and added this information:

```
Host gitlab.com
    Preferredauthentications publickey
    IdentityFile ~/.ssh/gitlab_com_rsa
```

Another way of doing this is specifying a user and changing the permissions of the file to be read and written to by the owner: `chmod 600 ~/.ssh/config`. However, the least amount of things that need to be done in order to make this work effectively is to use the config file and add those three lines.

## Solution 2

This solution involves invoking the ssh-agent whenever a user loads a terminal session. Add the following lines to your `~/.bashrc` or `~/.profile`:

    if [ -z "$SSH_AUTH_SOCK" ]; then
        eval `ssh-agent -s` > /dev/null 2>&1
        ssh-add "$HOME/.ssh/private_key_file
    fi

`$SSH_AUTH_SOCK` contains the path of the unix file socket the agent uses for communications. More on this topic can be found here: [Understanding ssh-agent and ssh-add](http://blog.joncairns.com/2013/12/understanding-ssh-agent-and-ssh-add/). You can manually run this by running your bashrc or profile files to test it out. The main problem with this is that with every shell you open, you will have another ssh-agent running. There is a way to use the same `$SSH_AUTH_SOCK` for every session, but it is much easier to use the SSH config file.

## Conclusion

The outcome of both of these methods is the same, but depending on how you want things to be done, you have options in how you want your keys to be added at any given point. My recommendation is to use the first solution, but if you want something quick and dirty, the second solution can also work for you. Please let me know if you have any questions by reaching out to me on Twitter [@WrittenByAPanda](https://twitter.com/writtenbyapanda).
