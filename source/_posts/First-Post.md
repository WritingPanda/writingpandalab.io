---
title: First Post
date: 2019-06-26 20:27:20
tags: 
---
I built this using `Hexo`. Normally, I would want to do this with `Pelican`, but I wanted to try something new. I also really like `Pug`, and `Hexo` supports that. 

Going to work on this theme here to make it look good with Pug templates. Yeah!
